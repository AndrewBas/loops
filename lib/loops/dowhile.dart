class DoWhile{
  int getIteratorResult(int breakpoint, int iterator){
    int iteratorResult = 0;

    do{
      iteratorResult += iterator;
    }
    while(iteratorResult < breakpoint);

    return iteratorResult;
  }
}