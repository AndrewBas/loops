class While{
  int getIteratorResult(int breakpoint, int iterator){
     int iteratorResult = 0;
     while(iteratorResult < breakpoint){
       iteratorResult += iterator;
     }
    return iteratorResult;
  }
}